# Description

This project aims to provide a _fast_ implementation of the algorithm 
for searching time coincidences between two streams of **ordered** data.

Two main parts are present:
- a C++ library (pycoinc) that exports the algorithm and data structures
  to the Python programming language
- a C++ program to calculate time coincidences between data in files
  specified via command line.

# How to build
The code can be built either directly via compiler command line or with
CMake. A C++11 compliant compiler is required.

Also, the following dependencies has to be present:
- Boost.Python library for C++-Python bindings
- libpython (usually present in normal Python installations)

## C++ compiler
```shell
g++ src/coincidence.cpp -o coincidences -O3 # The latter to activate compiler optimisations
```

## CMake
Make sure you have a working CMake environment (cmake-2.6+, GNU make, etc..).
The following commands will build the program in the `build/coincidences`:

```shell
mkdir build
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Release
make
```

# How to use
## Command line program
The program assumes that the provided files are in the EEE-specific CSV format.
Specify on the command line the path to the two files. The coincidence
window is set to 10µs.

```shell
./coincidences telescope1.csv telescope2.csv
```

## Python library
Make sure to add the path of the folder where pycoinc.so file is installed
to the `PYTHONPATH` environment variable:

```shell
export PYTHONPATH=$PYTHONPATH:/path/where/pycoinc.so/is/located/
```

Then, from the Python script/shell:

```python
import pycoinc as pc

coincidences = pc.find(array1, array2, delta)
```
