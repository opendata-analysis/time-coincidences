import pycoinc as pc
from sys import exit

a = [1, 11, 12, 14, 30, 32, 58, 62, 75]
b = [6, 16, 22, 29, 40, 50, 75, 79, 90]
expected = [
        ( 1,  6),
        (11,  6), (11, 16),
        (12,  6), (12, 16), (12, 22),
        (14,  6), (14, 16), (14, 22),
        (30, 22), (30, 29), (30, 40),
        (32, 22), (32, 29), (32, 40),
        (58, 50),
        (75, 75), (75, 79)]

t1 = pc.times()
t2 = pc.times()

t1.assign(a)
t2.assign(b)

c = pc.find(t1, t2, 10)
cn = pc.find_native(a, b, 10)

if not (c == cn == expected):
    print "Error: calculated array differs from expected"
    print "       find() =", c
    print "find_native() =", cn
    print "     expected =", expected
    exit(1)

