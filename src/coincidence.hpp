#include <vector>
#include <utility>
#include <algorithm>

namespace {
template<typename T>
T myabs(T x) {
  return x > 0 ? x : -x;
}
} // anon-ns

template<class IIterator1, class IIterator2, class T>
typename std::vector<std::pair<T, T>> find_coincidences(
    IIterator1 begin1
  , IIterator1 end1
  , IIterator2 begin2
  , IIterator2 end2
  , T delta
) {
  typename std::vector<std::pair<T, T>> coincidences;
  auto first_check = begin2;

  for (; begin1 != end1; ++begin1) {
    auto const t1 = *begin1;
    auto const begin = std::find_if(
        first_check
      , end2
      , [=](auto const& t2){return myabs(t1 - t2) <= delta;}
    );

    if (begin != end2) {
      auto const end = std::find_if(
          begin
        , end2
        , [=](auto const& t2){return myabs(t1 - t2) > delta;}
      );

      first_check = begin;

      std::for_each(
          begin
        , end
        , [&](auto t) {coincidences.push_back(std::make_pair(t1, t));}
      );
    }
  }

  return coincidences;
}
