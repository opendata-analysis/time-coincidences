#include <boost/python.hpp>
#include <coincidence.hpp>

boost::python::list pyfind_coincidences(
    std::vector<int64_t> const& c1
  , std::vector<int64_t> const& c2
  , boost::python::object delta
) {
  int64_t const d = boost::python::extract<int64_t>(delta);

  auto const coinc = find_coincidences(
      c1.begin()
    , c1.end()
    , c2.begin()
    , c2.end()
    , d
  );

  boost::python::list l;
  for (auto const& x : coinc) {
    l.append(boost::python::make_tuple(x.first, x.second));
  }

  return l;
}

boost::python::list pyfind_coincidences_native(
    boost::python::object l1
  , boost::python::object l2
  , boost::python::object delta
) {
  int64_t const d = boost::python::extract<int64_t>(delta);

  boost::python::stl_input_iterator<int64_t> b1(l1), b2(l2), end;
  std::vector<int64_t> c2(b2, end);

  auto const coinc = find_coincidences(
      b1
    , end
    , c2.begin()
    , c2.end()
    , d
  );

  boost::python::list l;
  for (auto const& x : coinc) {
    l.append(boost::python::make_tuple(x.first, x.second));
  }

  return l;
}

void assign_data(std::vector<int64_t>& v, boost::python::object obj)
{
  boost::python::stl_input_iterator<int64_t> begin(obj), end;
  v.assign(begin, end);
}

void append_data(std::vector<int64_t>& v, boost::python::object obj)
{
  boost::python::stl_input_iterator<int64_t> begin(obj), end;
  std::copy(begin, end, std::back_inserter(v));
}

BOOST_PYTHON_MODULE(pycoinc)
{
  boost::python::def(
      "find"
    , pyfind_coincidences
    , "Search coincidences between two ordered arrays, "
      "within the distance specified as the 3rd argument."
  );

  boost::python::def(
      "find_native"
    , pyfind_coincidences_native
    , "Search coincidences between two ordered arrays, "
      "within the distance specified as the 3rd argument. "
      "Generic Python iterables can be used."
  );

  boost::python::class_<std::vector<int64_t>>(
      "times"
    , "Class to represent times as C++ std::vector of 64-bit wide "
      "signed integers. It is meant to solely serve as input for the "
      "find function."
  ).def(
      "add"
    , &append_data
    , "Insert specified data at the end of the existing array."
  ).def(
      "assign"
    , &assign_data
    , "Replace the existing array with specified data."
  );
}
