#include <iostream>
#include <coincidence.hpp>
#include <string>
#include <vector>
#include <fstream>

std::vector<int64_t> read_time(std::istream& input)
{
  std::vector<int64_t> out;

  std::string line;
  std::getline(input, line);

  while (input && input.peek() != EOF) {
    std::getline(input, line, ',');
    std::getline(input, line, ',');
    int64_t const seconds = std::stoll(line);
    std::getline(input, line, ',');
    int64_t const nanoseconds = std::stoll(line);
    std::getline(input, line);

    out.push_back(seconds * 1000000000 + nanoseconds);
  }
  return out;
}

int main(int argc, char* argv[]) {
  std::ifstream input1(argv[1]);
  std::ifstream input2(argv[2]);

  std::vector<int64_t> times1 = read_time(input1);
  std::vector<int64_t> times2 = read_time(input2);

  int64_t const delta = 10000;

  auto const coincidences = find_coincidences(
      times1.begin()
    , times1.end()
    , times2.begin()
    , times2.end()
    , delta
  );

  for (auto const& x : coincidences) {
    std::cout << x.first << ' ' << x.second << ' ' << x.first - x.second << '\n';
  }
}

